from nwstore.model.modeldao import DomainDAO, BoxDAO, LineDAO, PortDAO


class DAOFactory:
    def __init__(self, session):
        self.session = session
        self.domaindao = DomainDAO(self)
        self.boxdao = BoxDAO(self)
        self.linedao = LineDAO(self)
        self.portdao = PortDAO(self)

    def fromJSON(self, obj):
        type = self.domaindao.assertJSONobj(obj)
        if type == "domain":
            return self.domaindao.fromJSON(obj)
        elif type == "box":
            return self.boxdao.fromJSON(obj)
        elif type == "line":
            return self.linedao.fromJSON(obj)
        elif type == "port":
            return self.portdao.fromJSON(obj)
        else:
            raise Exception("not a valid domain JSON object:\n %s" % pp.pformat(obj))

    def getDomainDAO(self):
        return self.domaindao

    def getBoxDAO(self):
        return self.boxdao

    def getLineDAO(self):
        return self.linedao

    def getPortDAO(self):
        return self.portdao
