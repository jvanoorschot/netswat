from nwstore.model import Base
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

class Domain(Base):
    __tablename__ = 'domains'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    boxes = relationship("Box", back_populates="domain")

    def __repr__(self):
        return "<Domain(name='%s')>" % (self.name)


class Box(Base):
    __tablename__ = 'boxes'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    domain_id = Column(Integer, ForeignKey('domains.id'))
    domain = relationship("Domain", back_populates="boxes")

    def __repr__(self):
        return "<Box(name='%s')>" % (self.name)


class Line(Base):
    __tablename__ = 'lines'
    id = Column(Integer, primary_key=True)
    name = Column(String)

    def __repr__(self):
        return "<Line(name='%s')>" % (self.name)


class Port(Base):
    __tablename__ = 'ports'
    id = Column(Integer, primary_key=True)
    name = Column(String)

    def __repr__(self):
        return "<Port(name='%s')>" % (self.name)

