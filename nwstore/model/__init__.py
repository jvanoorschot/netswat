from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()
from nwstore.model.dbobjects import Port, Line, Box, Domain
from nwstore.model.daofactory import DAOFactory