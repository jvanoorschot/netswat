import pprint
from nwstore.model import Domain, Box , Port, Line


class AbstractDAO(object):

    def __init__(self, factory):
        self.factory = factory
        super(AbstractDAO, self).__init__()

    def create(self):
        raise Exception("DAO needs to implement: create")

    def get(self):
        raise Exception("DAO needs to implement: create")

    def update(self):
        raise Exception("DAO needs to implement: create")

    def delete(self):
        raise Exception("DAO needs to implement: create")

    def fromJSON(self, obj):
        raise Exception("DAO needs to implement: fromJSON")

    def validid(self, id):
        if not id or id == None:
            return False
        return True

    def assertJSONobj(self, obj):
        if not("type" in obj) or not("name" in obj and self.validid(obj["name"])):
            pp = pprint.PrettyPrinter(width=41, compact=True)
            raise Exception("not a valid domain JSON object:\n %s" % pp.pformat(obj))
        else:
            return obj["type"]

    def assertJSONtype(self, obj, type):
        if not("type" in obj and obj["type"] == type) or not("name" in obj and self.validid(obj["name"])):
            pp = pprint.PrettyPrinter(width=41, compact=True)
            raise Exception("not a valid domain JSON object:\n %s" % pp.pformat(obj))

    def assertJSONcontent(self, obj, attribs):
        for attrib in attribs:
            if not attrib in obj:
                raise Exception("missing attribute in JSON object: %s" %attrib)
        for attrib in obj:
            if attrib != "type" and attrib != "name" and not attrib in attribs:
                raise Exception("extra attribute in JSON object: %s" % attrib)


class DomainDAO(AbstractDAO):
    def __init__(self, factory):
        super(DomainDAO, self).__init__(factory)

    class Builder():
        def __init__(self,factory, name):
            self.factory = factory
            self.name = name
            self.boxes = []
        def addBox(self,box):
            self.boxes.append(box)
        def build(self):
            item = Domain()
            item.name = self.name
            self.factory.session.add(item)
            return item

    def fromJSON(self, obj):
        self.assertJSONtype(obj, 'domain')
        self.assertJSONcontent(obj,['boxes'])
        builder = DomainDAO.Builder(self.factory, obj["name"])
        for boxobj in obj["boxes"]:
            box = self.factory.fromJSON(boxobj)
            builder.addBox(box)
        return  builder.build()


class BoxDAO(AbstractDAO):
    def __init__(self, factory):
        super(BoxDAO, self).__init__(factory)

    class Builder():
        def __init__(self,factory,name):
            self.factory = factory
            self.name = name
            self.ports = []
        def addPort(self,port):
            self.ports.append(port)
        def build(self):
            item = Box()
            item.name = self.name
            self.factory.session.add(item)
            return item

    def fromJSON(self, obj):
        self.assertJSONtype(obj, 'box')
        self.assertJSONcontent(obj,['ports'])
        builder = BoxDAO.Builder(self.factory,obj["name"])
        for portobj in obj["ports"]:
            port = self.factory.fromJSON(portobj)
            builder.addPort(port)
        return  builder.build()


class PortDAO(AbstractDAO):
    def __init__(self, factory):
        super(PortDAO, self).__init__(factory)

    class Builder():
        def __init__(self, factory, name):
            self.factory = factory
            self.name = name
        def build(self):
            item = Port()
            item.name = self.name
            self.factory.session.add(item)
            return item

    def fromJSON(self, obj):
        self.assertJSONtype(obj, 'port')
        builder = PortDAO.Builder(self.factory,obj["name"])
        return builder.build()


class LineDAO(AbstractDAO):
    def __init__(self, factory):
        super(LineDAO, self).__init__(factory)

    class Builder():
        def __init__(self,factory,name):
            self.factory = factory
            self.name = name
        def build(self):
            item = Line()
            item.name = self.name
            self.factory.session.add(item)
            return item

    def fromJSON(self, obj):
        self.assertJSONtype(obj, 'line')
        builder = LineDAO.Builder(self.factory,obj["name"])
        return builder.build()
