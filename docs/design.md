# Network simulation

* A Domain contains Nodes and selected STPs
* A Connection is either a pairing of STPs or an ordered set of Connections
* A Run is based on a Layout
* Runs are manipulated and each State remembered
* During a Run, Connections are created and deleted. This is added to the Run.
* Every object has a foreign key
* A Layout is created by one REST call (no manipulation)
* a Run is created and closed by REST calls
* Events are triggered by REST calls
* A REST call carries the time, which is used by the simulation to maintain time
* An empty rest call can progres time.
* Based on a fixed network Layout of Nodes with STP and Links
