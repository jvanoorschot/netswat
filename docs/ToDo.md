# A ToDo list 

0. create the REST triggered model node 
1. create the REST triggered simulation runner (parts can also be done as cmdline app)
2. create a tornado based, Cytoscape.js based (see /data/src) visualization
   
# Small steps
1. Change the to the tornado
2. Create an initial tornado/REST based model node, listening to its own port
3. Create an initial twistar database layout (check Sonja's model for ideas)
4. from and to json
  