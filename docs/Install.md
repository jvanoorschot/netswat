# Database Installation

Netswat requires a database to run. The following commands will create
the database, the user and such:

```bash
sudo -u postgres psql
sudo -u postgres createuser netswat -w
sudo -u postgres createdb -O netswat netswat
psql -U netswat netswat
```
After this, we can create the database:
```bash
psql -U netsdwat < docs/schema.sql
```
The schema.sql looks like this:

```postgresql
```