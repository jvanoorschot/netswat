from context import netswat
import unittest


class TestBasicRest(unittest.TestCase):

    def setUp(self):
        from sqlalchemy import create_engine
        import nwstore.model as model
        self.engine = create_engine('postgresql://netswat@localhost:5432/netswat')
        #self.engine = create_engine('sqlite:///:memory:', echo=True)
        model.Base.metadata.create_all(self.engine)
        self.assertIsNotNone(self.engine)

    def tearDown(self):
        pass

    def test_start(self):
        pass

if __name__ == '__main__':
    unittest.main()
