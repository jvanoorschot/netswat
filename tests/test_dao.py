import json
from tests.context import netswat
import unittest
from sqlalchemy.orm import sessionmaker
from nwstore.model import DAOFactory


class TestBasicDAO(unittest.TestCase):

    obj1_str = '{' \
               '"type": "domain",' \
               '"name": "mydomain",' \
               '"boxes": [' \
               '    {' \
               '    "type": "box",' \
               '    "name": "box1",' \
               '    "ports": [' \
               '       {' \
               '         "type": "port",' \
               '         "name": "port1"' \
               '       }' \
               '      ]' \
               '    }' \
               '  ]' \
               '}'

    def setUp(self):
        # create an empty database
        from sqlalchemy import create_engine
        import nwstore.model as model
        self.engine = create_engine('postgresql://netswat@localhost:5432/netswat')
        # self.engine = create_engine('sqlite:///:memory:', echo=True)
        model.Base.metadata.create_all(self.engine)
        self.assertIsNotNone(self.engine)

    def tearDown(self):
        pass

    def test_loadjson(self):
        objdef = json.loads(TestBasicDAO.obj1_str)
        self.assertEqual(len(objdef),3)
        self.assertEqual(objdef['type'], "domain")
        self.assertEqual(objdef['name'], "mydomain")
        self.assertIsNotNone(objdef)

    def test_create101(self):
        objdef = json.loads(TestBasicDAO.obj1_str)
        Session=sessionmaker()
        Session.configure(bind=self.engine)
        session = Session()
        factory = DAOFactory(session)
        self.assertIsNotNone(factory)
        domain = factory.fromJSON(objdef)
        self.assertIsNotNone(domain)
        session.add(domain)
        session.commit()
        session.flush()
        print("test")

if __name__ == '__main__':
    unittest.main()
